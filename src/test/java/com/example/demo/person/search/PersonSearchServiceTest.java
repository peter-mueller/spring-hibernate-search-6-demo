package com.example.demo.person.search;

import com.example.demo.DemoApplication;
import com.example.demo.person.Person;
import com.example.demo.person.PersonRepository;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = DemoApplication.class)
@ActiveProfiles("test")
class PersonSearchServiceTest {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    PersonSearchService personSearchService;

    @Test
    void search() {
        Person person = new Person();
        person.setName("Hans");
        Person savedPerson = personRepository.save(person);

        Page<Person> result = personSearchService.search(PageRequest.of(0, 10), "hans");

        Assertions.assertEquals(1, result.getTotalElements(), "The one person should be found");
        Person foundPerson = result.getContent().get(0);
        Assertions.assertEquals(savedPerson.getId(), foundPerson.getId(), "The person should have the correct ID");
        Assertions.assertEquals("Hans", foundPerson.getName(), "The person name should be correct");
    }
}