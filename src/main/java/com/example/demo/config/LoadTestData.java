package com.example.demo.config;


import com.example.demo.person.Person;
import com.example.demo.person.PersonRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.Arrays;

@Component
public class LoadTestData implements CommandLineRunner {
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private PersonRepository repo;


    @Override
    @Transactional
    public void run(java.lang.String... args) throws Exception {
        Person[] persons = objectMapper.readValue(new ClassPathResource("persons.json").getFile(), Person[].class);
        repo.saveAll(Arrays.asList(persons));
    }
}
