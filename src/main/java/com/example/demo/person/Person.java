package com.example.demo.person;

import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.GenericField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;

import javax.persistence.*;

@Entity
@Indexed
@Table(name = "PERSON")
public class Person {

    @Column(name = "ID")
    @Id
    @GeneratedValue
    private int id;

    @Column(name = "NAME")
    @FullTextField
    private String name;

    @Column(name = "ADDRESS")
    @FullTextField
    private String address;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
