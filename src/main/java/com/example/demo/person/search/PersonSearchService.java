package com.example.demo.person.search;

import com.example.demo.person.Person;
import com.example.demo.person.Person_;
import org.hibernate.search.engine.search.query.SearchResult;
import org.hibernate.search.mapper.orm.Search;
import org.hibernate.search.mapper.orm.session.SearchSession;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Service
public class PersonSearchService {

    @PersistenceContext
    EntityManager entityManager;

    @Transactional(readOnly = true)
    public Page<Person> search(Pageable pageable, String query) {
        SearchSession session = Search.session(entityManager);

        SearchResult<Person> result = session.search(Person.class)
                .where(
                        f -> f.match().fields(Person_.NAME, Person_.ADDRESS)
                                .matching(query)
                                .fuzzy(1)
                )
                .fetch((int) pageable.getOffset(), pageable.getPageSize());

        return new PageImpl<>(result.hits(), pageable, result.total().hitCount());
    }
}
