package com.example.demo.person.adapter;

import com.example.demo.person.Person;
import com.example.demo.person.search.PersonSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/person")
public class PersonController {

    @Autowired
    PersonSearchService searchService;

    @GetMapping
    public ResponseEntity<Page<Person>> searchPersons(Pageable pageable, @RequestParam("query") String query) {
        Page<Person> result = searchService.search(pageable, query);
        return ResponseEntity.ok(result);
    }
}
